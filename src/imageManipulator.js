export const toGrayscale = (image) =>
  image.map(column =>
    column.map(row => {
      const sum = row.reduce((acc, chValue, i) => acc + (i < 3 ? chValue : 0), 0)
      return Math.round(sum / 3)
    })
  )

export const toAscii = (image) => {
  const xSize = getImageSizeX(image)
  const ySize = getImageSizeY(image)
  let ascii = ''
  for (let y = 0; y < ySize; y++) {
    for (let x = 0; x < xSize; x++) {
      ascii += grayscaleToAscii(image[x][y])
    }
    if (y < ySize - 1) {
      ascii += '\n'
    }
  }
  return ascii
}

export const downscale = (image, xSize, ySize) => {
  const xScaleFactor = getImageSizeX(image) / xSize
  const yScaleFactor = getImageSizeY(image) / ySize
  const scaled = []
  for (let x = 0; x < xSize; x++) {
    scaled[x] = []
    for (let y = 0; y < ySize; y++) {
      const xImage = Math.round(x * xScaleFactor)
      const yImage = Math.round(y * yScaleFactor)
      scaled[x][y] = image[xImage][yImage]
    }
  }
  return scaled
}

const getImageSizeX = (image) =>
  image?.length ?? 0

const getImageSizeY = (image) =>
  image[0]?.length ?? 0

const grayscaleToAscii = (color) =>
  asciiChars[grayscaleToAsciiIndex(color)]

const grayscaleToAsciiIndex = (color) =>
  Math.min(9, Math.floor(color / 25.5))

const asciiChars =
  ['@', '&', '%', '#', '(', '/', '*', ',', '.', ' ']
