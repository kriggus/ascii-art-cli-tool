import { readImage } from './imageReader.js'
import { getInput, shouldShowHelp } from './argumentParser.js'
import { toGrayscale, toAscii, downscale } from './imageManipulator.js'

export const run = async (args, cwd, stdout, stderr) => {
  try {
    if (shouldShowHelp(args)) {
      stdout.write(helpStr)
    } else {
      const { path, xSize, ySize } = getInput(args, cwd)
      const image = await readImage(path)
      const grayscale = toGrayscale(image)
      const scaled = downscale(grayscale, xSize, ySize)
      const ascii = toAscii(scaled)

      stdout.write(`${ascii}\n\n`)
    }
  } catch (err) {
    stderr.write(`Oops, something went wrong:\n\n  ${err.message}\n`)
  }
}

const helpStr = `
ascii-art - command line ascii art generator [version 1.0.0]

Usage:  ascii-art [OPTIONS]... [FILE]

ascii-art is a tool for reading pictures from file, converting
them to ascii art and write the resulting artwork to stdout.

Required options are:
  --x-size      number of x-wise characters in output
  --y-size      number of y-wise characters in output

Source: https://bitbucket.org/kriggus/ascii-art-cli-tool/src/master/
`
