import getPixels from 'get-pixels'

export const readImage = async (filePath) =>
  new Promise((resolve, reject) => {
    getPixels(filePath, (err, pixels) => {
      if (err) {
        reject(err)
      } else {
        resolve(to3DArray(pixels))
      }
    })
  })

const to3DArray = (pixels) => {
  const xSize = pixels.shape[0]
  const ySize = pixels.shape[1]
  const chSize = pixels.shape[2]
  const array = []
  for (let x = 0; x < xSize; x++) {
    array[x] = []
    for (let y = 0; y < ySize; y++) {
      array[x][y] = []
      for (let ch = 0; ch < chSize; ch++) {
        array[x][y][ch] = pixels.data[
          x * chSize + y * chSize * xSize + ch
        ]
      }
    }
  }
  return array
}
