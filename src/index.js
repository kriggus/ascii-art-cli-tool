#!/usr/bin/env node

import { run } from './runner.js'

run(process.argv.slice(2), process.cwd(), process.stdout, process.stderr)
