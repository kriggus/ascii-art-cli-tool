export const getInput = (args, cwd) => {
  if (args.length < 5) {
    throw new Error('Missing required arguments')
  }
  return {
    path: getPath(args, cwd),
    xSize: getCoord(args, '--x-size'),
    ySize: getCoord(args, '--y-size')
  }
}

export const shouldShowHelp = (args) =>
  indexOfOption(args, '--help') !== -1

const getPath = (args, cwd) => {
  const relativePath = args[args.length - 1]
  if (!relativePath) {
    throw new Error('Missing FILE')
  }
  return `${cwd}/${relativePath}`
}

const getCoord = (args, option) => {
  const index = indexOfOption(args, option)
  if (index === -1) {
    throw new Error(`Missing option ${option}`)
  }
  const strValue = args[index + 1]
  if (!strValue) {
    throw new Error(`Missing value of option ${option}`)
  }
  const value = parseInt(strValue)
  if (!value || isNaN(value) || !isFinite(value) || value < 0) {
    throw new Error(`Invalid argument value ${strValue} for ${option}`)
  }
  return value
}

const indexOfOption = (args, option) =>
  args.findIndex(arg => arg === option)
