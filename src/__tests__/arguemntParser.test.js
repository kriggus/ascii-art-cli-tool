import { getInput } from './../argumentParser.js'

describe('argumentParser', () => {
  describe('getInput', () => {
    it('should return x, y options and file path', () => {
      const input1 = getInput(['--x-size', '44', '--y-size', '88', 'image.png'], 'root')
      const input2 = getInput(['--y-size', '88', '--x-size', '44', 'image.png'], 'root')

      const expected = {
        path: 'root/image.png',
        xSize: 44,
        ySize: 88
      }

      expect(input1).toEqual(expected)
      expect(input2).toEqual(expected)
    })

    it('should throw error when missing file path', () => {
      expect(() =>
        getInput(['--x-size', '44', '--y-size', '44'], 'root')
      ).toThrow(Error)
    })

    it('should throw error when missing x-size option', () => {
      expect(() =>
        getInput(['44', '--y-size', '44', 'pic.png'], 'root')
      ).toThrow(Error)
    })

    it('should throw error when missing x-size value', () => {
      expect(() =>
        getInput(['--x-size', '--y-size', '44', 'pic.png'], 'root')
      ).toThrow(Error)
    })

    it('should throw error when missing y size option', () => {
      expect(() =>
        getInput(['--x-size', '44', '44', 'pic.png'], 'root')
      ).toThrow(Error)
    })

    it('should throw error when missing y size value', () => {
      expect(() =>
        getInput(['--x-size', '44', '--y-size', 'pic.png'], 'root')
      ).toThrow(Error)
    })
  })
})
