import { readImage } from '../imageReader.js'

describe('imageReader', () => {
  describe('readImage', () => {
    it('should read image into 3d array', async () => {
      const filePath = `${process.cwd()}/src/__tests__/imageReaderInput1.png`
      const pixels = await readImage(filePath)

      expect(pixels).toEqual([
        [
          [255, 0, 0, 255],
          [0, 0, 0, 255]
        ],
        [
          [0, 255, 0, 255],
          [255, 255, 255, 255]
        ],
        [
          [0, 0, 255, 255],
          [102, 102, 102, 255]
        ]
      ])
    })

    it('should throw error if file does nott exist', async () => {
      expect(async () => {
        await readImage('NOT/A/FILE.png').toThrow(Error)
      })
    })
  })
})
