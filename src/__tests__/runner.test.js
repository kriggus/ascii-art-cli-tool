import { run } from '../runner.js'

describe('runner', () => {
  describe('run', () => {
    let stdoutSpy
    let stdoutOutput
    let stderrSpy
    let stderrOutput

    beforeEach(() => {
      stdoutOutput = undefined
      stdoutSpy = {
        write: (msg) => {
          stdoutOutput = msg
        }
      }
      stderrOutput = undefined
      stderrSpy = {
        write: (msg) => {
          stderrOutput = msg
        }
      }
    })

    it('should print help for option --help', async () => {
      await run(['--help'], '/', stdoutSpy, stderrSpy)

      expect(stdoutOutput).toMatch(/^\nascii-art - command line ascii art generator/)
      expect(stderrOutput).toBeUndefined()
    })

    it('should print ascii art for valid options and FILE', async () => {
      const expected = `@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     
@@@@@@&&%%%###((///***,,...     

`
      await run(
        ['--x-size', '32', '--y-size', '16', 'src/__tests__/runnerInput1.png'],
        process.cwd(),
        stdoutSpy,
        stderrSpy
      )

      expect(stdoutOutput).toEqual(expected)
      expect(stderrOutput).toBeUndefined()
    })

    it('should print error ascii art for valid options and FILE', async () => {
      await run(['not-a-valid-option'], '/', stdoutSpy, stderrSpy)

      expect(stdoutOutput).toBeUndefined()
      expect(stderrOutput).toBeDefined()
    })
  })
})
