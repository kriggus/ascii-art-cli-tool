import { downscale, toAscii, toGrayscale } from './../imageManipulator.js'

describe('imageManipulator', () => {
  describe('toGrayscale', () => {
    it('should return grayscale image from color image', () => {
      const color = [
        [
          [255, 0, 0, 255],
          [0, 0, 0, 255]
        ],
        [
          [155, 100, 0, 255],
          [255, 255, 255, 255]
        ],
        [
          [1, 200, 255, 255],
          [102, 102, 102, 255]
        ]
      ]
      const expectedGrayscale = [
        [85, 0],
        [85, 255],
        [152, 102]
      ]
      const actualGrayscale = toGrayscale(color)

      expect(actualGrayscale).toEqual(expectedGrayscale)
    })

    it('should return empty array from empty input', () => {
      expect(toGrayscale([])).toEqual([])
    })
  })

  describe('toAscii', () => {
    it('should return ascii representation of grayscale image', () => {
      const grayscale = [
        [0, 70, 140, 210],
        [10, 80, 150, 220],
        [20, 90, 160, 230],
        [30, 100, 170, 240],
        [40, 110, 180, 250],
        [50, 120, 190, 255],
        [60, 130, 200, 255]
      ]
      const expected = '@@@&&&%\n%###((/\n//**,,,\n..     '

      expect(toAscii(grayscale)).toEqual(expected)
    })

    it('should return empty string of grayscale image', () => {
      expect(toAscii([])).toEqual('')
    })
  })

  describe('downscale', () => {
    const grayscale = [
      [85, 0, 12, 43],
      [85, 255, 44, 12],
      [152, 102, 102, 66],
      [100, 100, 100, 0],
      [86, 221, 0, 0],
      [34, 34, 0, 10]
    ]

    it('should return downscaled 5x4 image', () => {
      const expectedScaled = [
        [85, 0, 12, 43],
        [85, 255, 44, 12],
        [152, 102, 102, 66],
        [86, 221, 0, 0],
        [34, 34, 0, 10]
      ]
      const scaled = downscale(grayscale, 5, 4)

      expect(scaled).toEqual(expectedScaled)
    })

    it('should return downscaled 4x3 image', () => {
      const expectedScaled = [
        [85, 0, 43],
        [152, 102, 66],
        [100, 100, 0],
        [34, 34, 10]
      ]
      const scaled = downscale(grayscale, 4, 3)

      expect(scaled).toEqual(expectedScaled)
    })

    it('should return downscale 1x1 image', () => {
      const scaled = downscale(grayscale, 1, 1)
      expect(scaled).toEqual([[85]])
    })

    it('should return original image if input dimensions match image dimensions', () => {
      const scaled = downscale(grayscale, 6, 4)
      expect(scaled).toEqual(grayscale)
    })

    it('should return empty array from empty input', () => {
      expect(downscale([])).toEqual([])
    })
  })
})
