# README #

ascii-art - command line ascii art generator.

### Setup ###

Developed with node v16.13.0 and npm 8.1.0.

Checkout and run `npm install`. To test `npm run start-example`.

### Install ###

To install run `npm install -g .`

If you get EACCES: https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally

### Usage ###

Installed:  
`ascii-art --x-size [INT] --y-size [INT] [FILE]`
  
npm:  
`npm start -- --x-size [INT] --y-size [INT] [FILE]`

Options:  
**--x-size** number of x-wise characters in output  
**--y-size** number of y-wise characters in output  
**--help** print help to stdout
